# Translucent Skins
For use with **LabyMod 1.8.9 only**.  
This is a port of the [Skin Blend Fix](https://github.com/RoccoDev/SkinBlendFix) mod.

This addon backports a fix from Minecraft 1.9 that correctly adds opacity (alpha) to the second layer of skins.

## Mod Conflicts
See [MOD-DEVELOPERS.md](MOD-DEVELOPERS.md) for info on how this addon works.  
If you're a mod developer and this causes a conflict with your mod, feel free to open an issue.
